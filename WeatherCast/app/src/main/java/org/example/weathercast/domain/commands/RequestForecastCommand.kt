package org.example.weathercast.domain.commands

import org.example.weathercast.data.ForecastRequest
import org.example.weathercast.domain.mappers.ForecastDataMapper
import org.example.weathercast.domain.model.ForecastList

class RequestForecastCommand(private val zipCode: String) : Command<ForecastList> {

	override fun execute(): ForecastList {
		val forecastRequest = ForecastRequest(zipCode)
		return ForecastDataMapper().convertFromDataModel(forecastRequest.execute())
	}
}