package org.example.weathercast.domain.commands

interface Command<out T> {
	fun execute(): T
}